
var events = require('events');
var stream = require('stream');
var util = require('util');


function LineStream(opts) {
	opts = opts || {};
	opts.decodeString = false;
	stream.Writable.call(this,opts);
	this.buffer = '';
}


util.inherits(LineStream, stream.Writable);

LineStream.prototype._write = function(data, enc, done) {
	data = data ? data.toString() : '';
	var idx = data.indexOf('\n');	
	while (idx != -1) {
		var line = this.buffer + data.substring(0,idx);
		this.emit('line', line);
		this.buffer = '';
		if (idx + 1 < data.length) {
			data = data.substring(idx + 1);
		} else {
			data = '';
		}
		idx = data.indexOf('\n');
	}

	if (data.length > 0) {
		this.buffer += data;
	}
	done();
}

module.exports.LineStream = LineStream;