var util = require('util');


var prefixes = {};
var line = '';
process.stdin.setEncoding('utf8');
process.stdin.on('data', function(data) { 
	var idx = data.indexOf('\n');
	while (idx != -1) {
		processLine(line + data.substring(0,idx), prefixes);		
		line = '';
		if ((idx + 1) < data.length) {
			data = data.substring(idx + 1);
		} else {
			data = '';
		}
		idx = data.indexOf('\n');
	}
	line += data;
});

process.stdin.on('end', function() {
	process.stdout.write('Prefix, Description, Minutes, Cost, Rate\n');
	for (prefix in prefixes) {
		var data = prefixes[prefix];
		var line = util.format('"%s","%s","%s","%s","%s"\n', 
			prefix, 
			data['description'], 
			(data['duration'] / 100).toFixed(2), 
			(data['cost'] / 1000).toFixed(3),
			((data['cost'] / 1000) / 
				(data['duration'] / 100))
			.toFixed(5));
		process.stdout.write(line);
	}
});

function processLine(line, prefixes) {
	var fields = line.split('\t');
	if (fields.length != 4) {
		return;
	}

	var idx = 0;
	var prefix = fields[idx++];
	var description = fields[idx++];
	var cost = Number(fields[idx++]) * 1000;
	var duration = Number(fields[idx++]) * 100;
	var data = prefixes[prefix];
	if (data == null) {
		data = prefixes[prefix] = { 
			description : description, 
			duration : 0.0,
			cost : 0.0
		};
	}
	data['duration'] += duration;
	data['cost'] += cost;
}