var common = require('./common');
var fs = require('fs');
var readline = require('readline');
var util = require('util');

var args = process.argv;
if (args.length < 3) {
	console.error('Usage: <prefix file>');
	process.exit(1);
}

var devNull = fs.createWriteStream('/dev/null');
loadPrefixMap(args[2], resolvePrefices);


var prefixMap;

function resolvePrefices() {
	var reader = new readline.createInterface( { input : process.stdin, output : devNull } );
	var regex = /(?:tel:)?(?:[ +])?(\d+).*/;
	var summary = {};

	reader.on('line', function onLine(line) { 
		line = line.trim();
		var args = line.split('\t');
		var idx = 0;
		var id = args[idx++];
		var dialString = args[idx++];
		var accountId = args[idx++];
		var duration = args[idx++];
		var prefix = '';
		if (dialString) {
			var matches = dialString.match(regex);
			if (matches && matches.length > 1) {
				var dialDigits = matches[1];
				if (dialDigits) {
					var prefixObject = resolve(prefixMap, dialDigits, 0);
					summarize(prefixMap, summary, prefixObject, accountId, duration);
				}
			}
		}
	});

	reader.on('close', function onClose() { 
		printSummary(summary);
	});
}

function summarize(prefixMap, summary, prefixObject, accountId, duration) {
	var accountMap = summary[accountId];
	if (!accountMap) {
		accountMap = summary[accountId] = {};
	}


	duration = parseFloat(duration);
	var adjustedDuration = (adjustDuration(duration, 6, 6) / 60.0);
	var prefix = (prefixObject ? prefixObject.prefix : '');
	var description = (prefixObject ? prefixObject.description : '');
	var rate = (prefixObject ? prefixObject.rate : 0.0);
	var prefixSummaryObject = accountMap[prefix];
	if (!prefixSummaryObject) {
		prefixSummaryObject = accountMap[prefix] = { 
			calls : 0,
			duration : 0.0, 
			adjustedDuration : 0.0,
			description : description, 
			rate : rate,
			cost : 0.0
		};
	}

	prefixSummaryObject.calls++;
	prefixSummaryObject.duration += duration;
	prefixSummaryObject.adjustedDuration += adjustedDuration;
	prefixSummaryObject.cost += (adjustedDuration * rate);
}

function printSummary(summary) {
	var out = process.stdout;
	out.write('Account\tPrefix\tCalls\tRaw Minutes\tAdjusted Minutes\tPredicted Cost\tPrefix Description\n');
	for (accountId in summary) {
		for (prefix in summary[accountId]) {
			var prefixObject = summary[accountId][prefix];
			var calls = prefixObject.calls;
			var duration = prefixObject.duration;
			var adjustedDuration = prefixObject.adjustedDuration;
			var cost = prefixObject.cost;
			var description = prefixObject.description;
			out.write(accountId);
			out.write('\t');
			out.write(prefix);
			out.write('\t');
			out.write(calls.toString());
			out.write('\t');
			out.write((duration / 60.0).toFixed(2));
			out.write('\t');
			out.write(adjustedDuration.toFixed(2));
			out.write('\t');
			out.write(cost.toFixed(3));
			out.write('\t');
			out.write(description);
			out.write('\n');
		}
	}
}

function resolve(map, dialString, idx) {
	if (idx == dialString.length) {
		return null;
	}

	var dd = dialString.charAt(idx);
	var subMap = map[dd];
	if (subMap) {
		var result = resolve(subMap, dialString, idx + 1);
		if (result) {
			return result;
		}

		if (subMap.match) {
			return { 
				prefix : subMap.prefix, 
				description : subMap.description,
				rate : subMap.rate
			};
		}
	}
	return null;
}

function loadPrefixMap(path, done) {
	var map = prefixMap = {};
	var prefixReader = new readline.createInterface( { input : fs.createReadStream(path), output : devNull } );
	function onLine(line) {
		line = line.trim();
		var fields = line.split('\t');
		var idx = 0;
		prefix = fields[idx++];
		description = fields[idx++];
		rate = parseFloat(fields[idx++]);
		var curMap = map;
		for (var idx = 0; idx < prefix.length; idx++) {
			var dd = prefix.charAt(idx);
			var subMap = curMap[dd];
			if (!subMap) {
				subMap = { };
				for (var nn = 0; nn <= 9; nn++) {
					subMap[nn.toString()] = { };
				}
				curMap[dd] = subMap;
			}
			curMap = subMap;
		}
		curMap.match = true;
		curMap.prefix = prefix;
		curMap.description = description;
		curMap.rate = rate;
	}
	prefixReader.on('line', onLine);
	prefixReader.on('close', done);
}

function adjustDuration(rawSeconds, minSeconds, intervalSeconds) {
	if (rawSeconds < minSeconds) {
		return minSeconds;
	} else {
		var remainder = (rawSeconds % intervalSeconds);
		if (remainder > 0) {
			return rawSeconds + intervalSeconds - remainder;
		} else {
			return rawSeconds;
		}
	}
}

