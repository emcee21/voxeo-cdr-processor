@GrabConfig(systemClassLoader=true)
@GrabResolver(name='tropo', root='http://maven.voxeolabs.net:8081/nexus/content/groups/public/')
@Grab(group='com.microsoft.sql', module='jdbc', version='3.0')
import groovy.sql.Sql


def idx = 0
def startDate = args[idx++]
def endDate = args[idx++]
def out = System.out
if (args.length > 2) {
	out = new PrintStream(new FileOutputStream(args[idx++]))
}

sql = Sql.newInstance('jdbc:sqlserver://labsdb101.jyo.voxeo.net:1433;databaseName=cassius_tropo',
	'reporting', 'reporting', 'com.microsoft.sqlserver.jdbc.SQLServerDriver')

sql.eachRow('select * from Cdr where startTime >= ? and startTime < ?', [startDate, endDate] ) {row->
	out.print (row.CallID ?: '')
	out.print '\t'
	out.print (row.ParentCallID ?: '')
	out.print '\t'
	out.print (row.SessionID ?: '')
	out.print '\t'
	out.print (row.ParentSessionID ?: '')
	out.print '\t'
	out.print (row.SipSessionID ?: '')
	out.print '\t'
	out.print (row.AccountID ?: '')
	out.print '\t'
	out.print (row.ApplicationID ?: '')
	out.print '\t'
	out.print (row.PPID ?: '')
	out.print '\t'
	out.print (row.StartTime ?: '')
	out.print '\t'
	out.print (row.EndTime ?: '')
	out.print '\t'
	out.print (row.Duration ?: '')
	out.print '\t'
	out.print (row.Outbound ?: '')
	out.print '\t'
	out.print (row.Status ?: '')
	out.print '\t'
	out.print (row.Network ?: '')
	out.print '\t'
	out.print (row.Channel ?: '')
	out.print '\t'
	out.print (row.StartUrl ?: '')
	out.print '\t'
	out.print (row.CalledID ?: '')
	out.print '\t'
	out.print (row.CallerID ?: '')
	out.print '\t'
	out.print (row.ServiceID ?: '')
	out.print '\t'
	out.print (row.PhoneNumberSid ?: '')
	out.print '\t'
	out.print (row.Disposition ?: '')
	out.print '\t'
	out.print (row.RecordingDuration ?: '')
	out.print '\t'
	out.print (row.DateCreated ?: '')
	out.print '\t'
	out.print (row.BrowserIP ?: '')
	out.print '\t'
	out.print (row.ScriptThrowable ?: '')
	out.print '\t';
	out.print (row.applicationType ?: '')
	out.print '\n'
}
