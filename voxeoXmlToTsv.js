var expat = require('node-expat'),
	fs = require('fs'),	
	util = require('util');


var cost, duration;
var parser = new expat.Parser('UTF-8');

var sessionAttrs, dialAttrs;
parser.on('startElement', function(name, attrs) { 
	var prefix, description;	
	switch (name) {
		case 'session':
		sessionAttrs = attrs;
		break;

		case 'outboundDialString':
		dialAttrs = attrs;
		break;
	}
});



// <session applicationId='465725' applicationName='Number Holder - DO NOT REMOVE' startTime='02:46:41' startDate='2013-07-01' 
//    type='CCXML' direction='outbound' platformRate='0.0000' transferCharges='0.0' transportCharges='0.064' platformCharges='0.000'
//    totalCharges='0.064' calledId='+96171807199' callerId='' sessionId='311f0c3dc8dd5c793e2469f21c9209c5' partitionPlatformId='593' 
//    durationMinutes='0.20' bridged='false' connectionId='0' connectionNumber='0' recordingCharges='0.000' conferencingCharges='0.000' 
//    payphoneCharges='0.000'>
//         <url/>
//         <outboundDialString dialString='96171807199' prefix='96171' description='LEBANON MOBILE'/>
//     </session>


var out = process.stdout;
parser.on('endElement', function(name,attrs) { 
	if (name == 'session') {
		var attrs = sessionAttrs;
		out.write(attrs['sessionId']);
		out.write('\t');
		out.write(attrs['connectionNumber']);
		out.write('\t');
		out.write(attrs['type']);
		out.write('\t');
		out.write(attrs['startDate'])
		out.write(' ');
		out.write(attrs['startTime']);
		out.write('\t');
		out.write(attrs['direction']);
		out.write('\t');
		out.write(attrs['durationMinutes']);
		out.write('\t');
		out.write(attrs['platformRate']);
		out.write('\t');
		out.write(attrs['platformCharges']);
		out.write('\t');
		out.write(attrs['transferCharges']);
		out.write('\t');
		out.write(attrs['transportCharges']);
		out.write('\t');
		out.write(attrs['recordingCharges']);
		out.write('\t');
		out.write(attrs['totalCharges']);
		out.write('\t');
		out.write(attrs['callerId']);
		out.write('\t');
		out.write(attrs['calledId']);
		out.write('\t');
		// out.write(attrs['parentSessionId']);
		out.write('\t');

		attrs = dialAttrs;
		if (attrs) {
			var prefix = attrs['prefix'] 
			if (prefix) {
				out.write(prefix);
			}
			out.write('\t');
			var description = attrs['description'] 
			if (description) {
				out.write(description);
			}
		} else {
			out.write('\t');
		}
		out.write('\n');	

		dialAttrs = sessionAttrs = null;
	}
});

process.stdin.pipe(parser);
